package com.ssg.lectorfeed;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

/**
 * Created by jorge on 22/02/16.
 */
public class SpecialAdapter extends SimpleAdapter {

    //private int[] colors = new int[] { 0x30FF0000, 0x300000FF };


    public SpecialAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = super.getView(position, convertView, parent);
        /*
        int colorPos = position % colors.length;
        view.setBackgroundColor(colors[colorPos]);
        */
        TextView title = (TextView) view.findViewById(android.R.id.text1);
        TextView link = (TextView) view.findViewById(android.R.id.text2);

        title.setTextColor(Color.parseColor("#212121"));
        link.setTextColor(Color.parseColor("#03A9F4"));
        return view;
    }
}
