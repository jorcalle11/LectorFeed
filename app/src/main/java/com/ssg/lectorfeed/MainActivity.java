package com.ssg.lectorfeed;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.HashMap;
import java.util.LinkedList;


public class MainActivity extends AppCompatActivity {

    static final String DATA_TITLE = "T";
    static final String DATA_LINK  = "L";
    static LinkedList<HashMap<String,String>> data;
    static String feedUrl = "http://www.maestrosdelweb.com/index.xml";
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button load = (Button) findViewById(R.id.buttonLoad);
        load.setOnClickListener(loadPost);

        ListView posts = (ListView) findViewById(R.id.listViewPosts);
        posts.setOnItemClickListener(redirectBrowser);
    }

    View.OnClickListener loadPost = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ListView posts = (ListView) findViewById(R.id.listViewPosts);

            if (posts.getAdapter()!= null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Ya se han cargado los datos, ¿Esta seguro de volverlos a cargar?")
                        .setCancelable(false)
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                loadData();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .create()
                        .show();
            } else {
                loadData();
            }
        }
    };

    AdapterView.OnItemClickListener redirectBrowser = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            HashMap<String,String> post = data.get(position);
            Intent browserAction = new Intent(Intent.ACTION_VIEW, Uri.parse(post.get(DATA_LINK)));
            startActivity(browserAction);
        }
    };

    private void setData (LinkedList<HashMap<String,String>> data) {
        SpecialAdapter sAdapter = new SpecialAdapter(getApplicationContext(), data, android.R.layout.two_line_list_item, new String[] {DATA_TITLE,DATA_LINK}, new int[] {android.R.id.text1, android.R.id.text2});
        ListView posts = (ListView) findViewById(R.id.listViewPosts);
        posts.setAdapter(sAdapter);
    }

    private final Handler progressHandler = new Handler(){
        @SuppressWarnings("unchecked")

        public void handleMessage (Message msg) {
            if (msg.obj != null) {
                data = (LinkedList<HashMap<String,String>>)msg.obj;
                setData(data);
            }
            progressDialog.dismiss();
        }
    };

    private void loadData () {
        progressDialog = ProgressDialog.show(MainActivity.this, "","Cargando posts, Espere...",true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                XMLParser parser = new XMLParser(feedUrl);
                Message msg = progressHandler.obtainMessage();
                msg.obj = parser.parse();
                progressHandler.sendMessage(msg);
            }
        }).start();
    }
}
